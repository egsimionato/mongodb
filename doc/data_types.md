* String:
* Integer: (32-bit|64-bit)
* Boolean: (TRUE|FALSE)
* Double: floating-point values.
* Min/Max keys: to compare value lowest and highest BSON elements.
* Arrays: list or multiple values ["John, Smith", "Mark, Spencer"].
* Timestamp: store a timestamp. last modified, created.
* Object: storing embedded documents.
* Null: a null value.
* Symbol: a String, used by languages that use a specific symbol type.
* Date: current date or time in the Unix time (POSIX time format).
* Object ID: data type ti document's ID.
* Binary Data: a binary set of data.
* Regular Expression: used for regular expressions.
